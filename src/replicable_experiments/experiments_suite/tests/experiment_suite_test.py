import traceback
import unittest
import pandas as pd
from sklearn.model_selection import ParameterGrid
from experiments_suite.models.cases import DataSetCase
from experiments_suite.models.config_mongo import AlgorithmConfig, VectorizerConfig, FeatureExtractionConfig, \
    TransformerConfig, ExperimentConfig, DatasetConfig, ExperimentSuiteConfig
from experiments_suite.models.dataset import DataSet
from experiments_suite.models.experiment import Experiment
from experiments_suite.models.experiment_suite import ExperimentSuite
from experiments_suite.models.reports import ExperimentReport, ExperimentSuiteReport
from experiments_suite.utils import ClsType
from experiments_suite.db.mongo import conn
import logging


def split_df(df):
    import numpy as np
    malicious = df[df["class"] == -1].sample(frac=1)
    malicious["is_train"] = np.random.uniform(0, 1, len(malicious)) <= .75
    normal = df[df["class"] == 1].sample(frac=1)
    normal["is_train"] = np.random.uniform(0, 1, len(normal)) <= .75
    df = pd.concat([malicious, normal], ignore_index=True)
    df = df.sample(frac=1)  # shuffle

    return df[df["is_train"] == True], df[df["is_train"] == False], None, None


class ExperimentSuiteTests(unittest.TestCase):
    def setUp(self):
        loglevel = logging.DEBUG
        logging.basicConfig(level=loglevel)
        self.df = pd.DataFrame([[1, 2, 3], [5, 6, 7], [5, 6, 7], [2, 4, 3], [5, 5, 7]], columns=["A", "B", "class"])

    def test_simple_initialization(self):
        ExperimentSuite.objects.delete()
        Experiment.objects.delete()
        DataSet.objects.delete()
        cls_config = AlgorithmConfig(alg_type=ClsType.LinearSvm)
        vec_config = VectorizerConfig()
        feature_config = FeatureExtractionConfig(column_names=[''])
        dataset_config = DatasetConfig(class_col="class")
        transformer_config = TransformerConfig()

        dataset = DataSet(self.df)
        experiment_suite_cfg = ExperimentSuiteConfig()
        experiment_suite_cfg.alg_configs = [cls_config]
        experiment_suite_cfg.vec_configs = [vec_config]
        experiment_suite_cfg.ds_configs = [dataset_config]
        experiment_suite_cfg.feature_extraction_configs = [feature_config]
        experiment_suite_cfg.transformation_configs = [transformer_config]
        experiment_suite = ExperimentSuite(config=experiment_suite_cfg)
        experiment_suite.start(dataset)
        experiment_suite.save()
        dataset.save()

        self.assertEquals(ExperimentSuite.objects.all().count(), 1)
        self.assertEquals(Experiment.objects.all().count(), 1)
        # ExperimentSuite.objects.delete()
        # Experiment.objects.delete()
        # DataSet.objects.delete()

    def test_param_grid(self):
        df = pd.DataFrame([[1, 2, 3]], columns=["A", "B", "C"])
        dataset = DataSet(df)
        dataset_case = DataSetCase([DatasetConfig(class_col="class")], dataset)
        cls_config = AlgorithmConfig(alg_type=ClsType.LinearSvm)
        cls_config_2 = AlgorithmConfig(alg_type=ClsType.NBayes)
        vec_config = VectorizerConfig()
        feature_config = FeatureExtractionConfig(column_names=[''])
        dataset_config = DatasetConfig(class_col="class")
        transformer_config = TransformerConfig()
        e_cfg = ExperimentConfig(ds_config=dataset_config,
                                 alg_config=cls_config,
                                 feature_extraction_config=feature_config,
                                 vec_config=vec_config,
                                 transformer_config=transformer_config
                                 )
        params = {
            "alg_config": [cls_config, cls_config_2],
            "vec_config": [vec_config],
            "feature_extraction_config": [feature_config, feature_config],
            "ds_config": [dataset_config],
            "transformer_config": [transformer_config],
        }

        param_grid = ParameterGrid(params)
        Experiment.objects.delete()
        times = 0
        for g in param_grid:
            print g
            times += 1
            e_cfg = ExperimentConfig(**g)
            e = Experiment(config=e_cfg, dataset=dataset)
            e.save()

        self.assertEquals(Experiment.objects.all().count(), times)
        Experiment.objects.delete()

    def test_object_retrieval(self):
        log = logging.getLogger(__name__)
        global df
        # ExperimentSuite.objects.delete()
        # Experiment.objects.delete()
        # DataSet.objects.delete()
        cls_config = AlgorithmConfig(alg_type=ClsType.LinearSvm)
        vec_config = VectorizerConfig()
        feature_config = FeatureExtractionConfig(column_names=[''])
        dataset_config = DatasetConfig(class_col="class")
        transformer_config = TransformerConfig()

        dataset = DataSet(df)
        experiment_suite_cfg = ExperimentSuiteConfig()
        experiment_suite_cfg.alg_configs = [cls_config]
        experiment_suite_cfg.vec_configs = [vec_config]
        experiment_suite_cfg.ds_configs = [dataset_config]
        experiment_suite_cfg.feature_extraction_configs = [feature_config]
        experiment_suite_cfg.transformation_configs = [transformer_config]
        experiment_suite = ExperimentSuite(config=experiment_suite_cfg)
        experiment_suite.start(dataset)
        experiment_suite.save()
        dataset.save()

        self.assertGreaterEqual(ExperimentSuite.objects.all().count(), 1)
        self.assertGreaterEqual(Experiment.objects.all().count(), 1)
        log.debug("First experiment's object id:", experiment_suite.experiments[0].pk)
        retrieved = ExperimentSuite.objects.raw({"experiments": experiment_suite.experiments[0].pk
                                                 # {"$elemMatch":
                                                 #      {"_id": experiment_suite.experiments[0].pk
                                                 #       }
                                                 #  }
                                                 })
        print retrieved.count()
        self.assertEquals(retrieved.count(), 1)
        retrieved = Experiment.objects.raw({"_id": experiment_suite.experiments[0].pk})
        print retrieved.count()
        self.assertEquals(retrieved.count(), 1)

    def test_function_store_use_retrieve(self):
        global df
        # ExperimentSuite.objects.delete()
        # Experiment.objects.delete()
        # DataSet.objects.delete()
        cls_config = AlgorithmConfig(alg_type=ClsType.LinearSvm, alg_kwargs={"C": 10})
        vec_config = VectorizerConfig()
        feature_config = FeatureExtractionConfig(column_names=[''])
        dataset_config = DatasetConfig(class_col="class", split=split_df)
        transformer_config = TransformerConfig()

        dataset = DataSet(df)
        experiment_suite_cfg = ExperimentSuiteConfig()
        experiment_suite_cfg.alg_configs = [cls_config]
        experiment_suite_cfg.vec_configs = [vec_config]
        experiment_suite_cfg.ds_configs = [dataset_config]
        experiment_suite_cfg.feature_extraction_configs = [feature_config]
        experiment_suite_cfg.transformation_configs = [transformer_config]
        experiment_suite = ExperimentSuite(config=experiment_suite_cfg)
        experiment_suite.start(dataset)
        experiment_suite.save()
        dataset.save()
        print experiment_suite.experiments[0].clf.cls.C
        self.assertTrue(experiment_suite.experiments[0].clf.cls.C == 10)
        self.assertGreaterEqual(ExperimentSuite.objects.all().count(), 1)
        self.assertGreaterEqual(Experiment.objects.all().count(), 1)
        print experiment_suite.experiments[0].pk
        retrieved = ExperimentSuite.objects.raw({"experiments": experiment_suite.experiments[0].pk
                                                 # {"$elemMatch":
                                                 #      {"_id": experiment_suite.experiments[0].pk
                                                 #       }
                                                 #  }
                                                 })
        self.assertEquals(retrieved.count(), 1)
        retrieved.first().start(dataset)
        # ExperimentSuite.objects.delete()
        # Experiment.objects.delete()
        # DataSet.objects.delete()

    def test_multiple_experiments(self):
        global df
        ExperimentSuite.objects.delete()
        Experiment.objects.delete()
        DataSet.objects.delete()
        cls_config = AlgorithmConfig(alg_type=ClsType.LinearSvm, alg_kwargs={"C": 10})
        cls_config2 = AlgorithmConfig(alg_type=ClsType.NBayes)
        cls_config3 = AlgorithmConfig(alg_type=ClsType.Perceptron)
        vec_config = VectorizerConfig()
        feature_config = FeatureExtractionConfig(column_names=[''])
        dataset_config = DatasetConfig(class_col="class", split=split_df)
        dataset_config2 = DatasetConfig(class_col="class", split=0.9)
        transformer_config = TransformerConfig()

        dataset = DataSet(df)
        experiment_suite_cfg = ExperimentSuiteConfig()
        experiment_suite_cfg.alg_configs = [cls_config, cls_config2, cls_config3]
        experiment_suite_cfg.vec_configs = [vec_config]
        experiment_suite_cfg.ds_configs = [dataset_config, dataset_config2]
        experiment_suite_cfg.feature_extraction_configs = [feature_config]
        experiment_suite_cfg.transformation_configs = [transformer_config]
        experiment_suite = ExperimentSuite(config=experiment_suite_cfg)
        experiment_suite.start(dataset)
        experiment_suite.save()
        dataset.save()

        self.assertGreaterEqual(ExperimentSuite.objects.all().count(), 1)
        self.assertGreaterEqual(Experiment.objects.all().count(), 1)
        print experiment_suite.experiments[0].pk
        retrieved = ExperimentSuite.objects.raw({"experiments": experiment_suite.experiments[0].pk})
        self.assertEquals(retrieved.count(), 1)
        self.assertGreaterEqual(len(retrieved.first().experiments), 3)
        print len(retrieved.first().experiments)
        # ExperimentSuite.objects.delete()
        # Experiment.objects.delete()
        # DataSet.objects.delete()

    def test_metrics_aggregation(self):
        global df
        ExperimentSuite.objects.delete()
        Experiment.objects.delete()
        DataSet.objects.delete()
        cls_config = AlgorithmConfig(alg_type=ClsType.LinearSvm)
        cls_config2 = AlgorithmConfig(alg_type=ClsType.KMeans)
        cls_config3 = AlgorithmConfig(alg_type=ClsType.Perceptron)
        vec_config = VectorizerConfig()
        feature_config = FeatureExtractionConfig(column_names=[''])
        dataset_config = DatasetConfig(class_col="class", split=split_df)
        dataset_config2 = DatasetConfig(class_col="class", split=0.9)
        transformer_config = TransformerConfig()

        dataset = DataSet(df)
        experiment_suite_cfg = ExperimentSuiteConfig()
        experiment_suite_cfg.alg_configs = [cls_config, cls_config2, cls_config3]
        experiment_suite_cfg.vec_configs = [vec_config]
        experiment_suite_cfg.ds_configs = [dataset_config, dataset_config2]
        experiment_suite_cfg.feature_extraction_configs = [feature_config]
        experiment_suite_cfg.transformation_configs = [transformer_config]
        experiment_suite = ExperimentSuite(config=experiment_suite_cfg)
        experiment_suite.start(dataset)
        experiment_suite.save()
        dataset.save()

        self.assertGreaterEqual(ExperimentSuite.objects.all().count(), 1)
        self.assertGreaterEqual(Experiment.objects.all().count(), 1)
        print experiment_suite.experiments[0].pk
        retrieved = ExperimentSuite.objects.raw({"experiments": experiment_suite.experiments[0].pk})
        self.assertEquals(retrieved.count(), 1)
        self.assertEquals(len(retrieved.first().experiments), 6)
        print len(retrieved.first().experiments)
        import numpy as np
        accuracies = []
        experiments = retrieved.first().experiments
        for e in experiments:
            print e.metrics.accuracy
            print ClsType(e.config.alg_config.alg_type).name
            accuracies.append(e.metrics.accuracy)
        print np.average(accuracies)
        print np.max(accuracies)
        print "Best Experiment:", ClsType(
            experiments[accuracies.index(np.max(accuracies))].config.alg_config.alg_type).name

        self.assertIn(ClsType(experiments[accuracies.index(np.max(accuracies))].config.alg_config.alg_type).name,
                      ["LinearSvm", "Perceptron", "KMeans"])

    def test_resuming(self):
        log = logging.getLogger(__name__)
        global df
        ExperimentSuite.objects.delete()
        Experiment.objects.delete()
        DataSet.objects.delete()
        cls_config = AlgorithmConfig(alg_type=ClsType.LinearSvm)
        cls_config2 = AlgorithmConfig(alg_type=ClsType.KMeans, alg_kwargs={"n_clusters": 2})
        cls_config3 = AlgorithmConfig(alg_type=ClsType.Perceptron)
        vec_config = VectorizerConfig()
        feature_config = FeatureExtractionConfig(column_names=[''])
        dataset_config = DatasetConfig(class_col="class", split=split_df)
        dataset_config2 = DatasetConfig(class_col="class", split=0.9)
        transformer_config = TransformerConfig()

        dataset = DataSet(df)
        experiment_suite_cfg = ExperimentSuiteConfig(resume=True)
        experiment_suite_cfg.alg_configs = [cls_config, cls_config2, cls_config3]
        experiment_suite_cfg.vec_configs = [vec_config]
        experiment_suite_cfg.ds_configs = [dataset_config, dataset_config2]
        experiment_suite_cfg.feature_extraction_configs = [feature_config]
        experiment_suite_cfg.transformation_configs = [transformer_config]
        experiment_suite = ExperimentSuite(config=experiment_suite_cfg)
        from interruptingcow import timeout
        try:
            with timeout(2, exception=RuntimeError):
                experiment_suite.start(dataset)
        except RuntimeError:
            log.warn("Moooo!")
            pass
        experiment_suite.save()
        dataset.save()

        self.assertGreaterEqual(ExperimentSuite.objects.all().count(), 1)
        self.assertGreaterEqual(Experiment.objects.all().count(), 1)
        retrieved = ExperimentSuite.objects.raw({"experiments": experiment_suite.experiments[0].pk})
        self.assertEquals(retrieved.count(), 1)
        self.assertLess(len(retrieved.first().experiments), 6)
        e_suite = retrieved.first()
        self.assertEquals(e_suite.config.resume, True)
        log.warn("No of experiments completed so far:" + str(len(e_suite.experiments)))
        e_suite.start(dataset)
        log.warn("No of experiments after resume:" + str(len(e_suite.experiments)))
        e_suite.save()
        self.assertEquals(len(e_suite.experiments), 6)

    def test_reports(self):

        global df
        ExperimentSuite.objects.delete()
        Experiment.objects.delete()
        DataSet.objects.delete()
        cls_config = AlgorithmConfig(alg_type=ClsType.LinearSvm)
        cls_config2 = AlgorithmConfig(alg_type=ClsType.KMeans, alg_kwargs={"n_clusters": 2})
        cls_config3 = AlgorithmConfig(alg_type=ClsType.Perceptron)
        # vec_config = VectorizerConfig()
        # feature_config = FeatureExtractionConfig(column_names=[''])
        dataset_config = DatasetConfig(class_col="class", split=split_df)
        dataset_config2 = DatasetConfig(class_col="class", split=0.9)
        transformer_config = TransformerConfig()

        dataset = DataSet(df)
        experiment_suite_cfg = ExperimentSuiteConfig(resume=True)
        experiment_suite_cfg.alg_configs = [cls_config, cls_config2, cls_config3]
        # experiment_suite_cfg.vec_configs = [vec_config]
        experiment_suite_cfg.ds_configs = [dataset_config, dataset_config2]
        # experiment_suite_cfg.feature_extraction_configs = [feature_config]
        experiment_suite_cfg.transformation_configs = [transformer_config]
        experiment_suite = ExperimentSuite(config=experiment_suite_cfg)
        experiment_suite.start(dataset)
        experiment_suite.save()
        dataset.save()

        self.assertGreaterEqual(ExperimentSuite.objects.all().count(), 1)
        self.assertGreaterEqual(Experiment.objects.all().count(), 1)
        for e in experiment_suite.experiments:
            ExperimentReport(e).report()

        ExperimentSuiteReport(experiment_suite).report()
        self.assertTrue(False)

    def test_parallelization(self):

        global df
        ExperimentSuite.objects.delete()
        Experiment.objects.delete()
        DataSet.objects.delete()
        cls_config = AlgorithmConfig(alg_type=ClsType.LinearSvm)
        cls_config2 = AlgorithmConfig(alg_type=ClsType.KMeans, alg_kwargs={"n_clusters": 2})
        cls_config3 = AlgorithmConfig(alg_type=ClsType.Perceptron)
        dataset_config = DatasetConfig(class_col="class", split=split_df)
        dataset_config2 = DatasetConfig(class_col="class", split=0.9)
        transformer_config = TransformerConfig()

        dataset = DataSet(df)
        experiment_suite_cfg = ExperimentSuiteConfig(resume=True, parallelize=True)
        experiment_suite_cfg.alg_configs = [cls_config, cls_config2, cls_config3]
        experiment_suite_cfg.ds_configs = [dataset_config, dataset_config2]
        experiment_suite_cfg.transformation_configs = [transformer_config]
        experiment_suite = ExperimentSuite(config=experiment_suite_cfg)
        try:
            # with timeout(20, exception=RuntimeError):
            experiment_suite.start(dataset)
        except Exception:
            traceback.print_exc()
            # log.warn("Moooo!")
            pass
        experiment_suite.save()
        dataset.save()

        self.assertGreaterEqual(ExperimentSuite.objects.all().count(), 1)
        self.assertGreaterEqual(Experiment.objects.all().count(), 1)
        for e in experiment_suite.experiments:
            ExperimentReport(e).report()

        ExperimentSuiteReport(experiment_suite).report()
        self.assertTrue(False)


if __name__ == '__main__':
    unittest.main()
    # suite = unittest.TestLoader().loadTestsFromTestCase(ExperimentSuiteTests)
    # test_result = unittest.TextTestRunner(verbosity=2).run(suite)
    # print test_result.errors
    # print test_result.failures
