import unittest
from experiments_suite.models.config_mongo import AlgorithmConfig, VectorizerConfig, FeatureExtractionConfig, DatasetConfig, \
    TransformerConfig, ExperimentConfig
from experiments_suite.models.experiment import Experiment
from experiments_suite.utils import Step
from experiments_suite.utils import ClsType
from experiments_suite.db.mongo import conn
import pandas as pd
from experiments_suite.models.dataset import DataSet


class ExperimentTests(unittest.TestCase):

    def setUp(self):
        self.df = pd.DataFrame([[1, 2, 3], [5, 6, 7], [5, 6, 7], [2, 4, 3], [5, 5, 7]], columns=["A", "B", "class"])

    def test_simple_save_mongo(self):
        cls_config = AlgorithmConfig(alg_type=ClsType.LinearSvm)
        vec_config = VectorizerConfig()
        feature_config = FeatureExtractionConfig(column_names=[''])
        dataset_config = DatasetConfig(class_col="class")
        transformer_config = TransformerConfig()
        e_cfg = ExperimentConfig(ds_config=dataset_config,
                                 alg_config=cls_config,
                                 feature_extraction_config=feature_config,
                                 vec_config=vec_config,
                                 transformer_config=transformer_config
                                 )
        dataset = DataSet(self.df)
        e = Experiment(config=e_cfg, dataset=dataset)
        e.orchestrate([
            Step.Vectorization,
            Step.Transformation,
            Step.Train,
            Step.Predict,
            Step.Evaluate,
            Step.Save
        ])

        dataset.save()
        e.save()
        print e.pk
        q = Experiment.objects.raw({"config.alg_config.alg_type": 0})
        print q.count()

        self.assertEquals(Experiment.objects.all().count(), 1)
        self.assertEquals(DataSet.objects.all().count(), 1)

        Experiment.objects.delete()
        DataSet.objects.delete()
        self.assertEquals(Experiment.objects.all().count(), 0)
        self.assertEquals(DataSet.objects.all().count(), 0)

    def test_proper_set_up(self):
        cls_config = AlgorithmConfig(alg_type=ClsType.LinearSvm)
        dataset_config = DatasetConfig(class_col="class")
        transformer_config = TransformerConfig()
        e_cfg = ExperimentConfig(ds_config=dataset_config,
                                 alg_config=cls_config,
                                 transformer_config=transformer_config
                                 )
        dataset = DataSet(self.df)
        e = Experiment(config=e_cfg, dataset=dataset)
        self.assertTrue(Step.Vectorization in e.step_to_action)
        self.assertTrue(Step.Transformation in e.step_to_action)
        self.assertTrue(Step.FeatureExtraction in e.step_to_action)
        self.assertTrue(Step.Predict in e.step_to_action)
        self.assertTrue(Step.Evaluate in e.step_to_action)
        self.assertTrue(Step.Save in e.step_to_action)

        self.assertTrue(callable(e.step_to_action[Step.Vectorization]))
        self.assertTrue(callable(e.step_to_action[Step.Transformation]))
        self.assertTrue(callable(e.step_to_action[Step.FeatureExtraction]))
        self.assertTrue(callable(e.step_to_action[Step.Predict]))
        self.assertTrue(callable(e.step_to_action[Step.Evaluate]))
        self.assertTrue(callable(e.step_to_action[Step.Save]))

    def test_orchestrate(self):
        cls_config = AlgorithmConfig(alg_type=ClsType.LinearSvm)
        dataset_config = DatasetConfig(class_col="class")
        transformer_config = TransformerConfig()
        e_cfg = ExperimentConfig(ds_config=dataset_config,
                                 alg_config=cls_config,
                                 transformer_config=transformer_config
                                 )
        dataset = DataSet(self.df)
        e = Experiment(config=e_cfg, dataset=dataset)
        e.orchestrate([
            Step.Vectorization,
            Step.Transformation,
            Step.Train,
            Step.Predict,
            Step.Evaluate,
            Step.Save
        ])

    def test_vectorize(self):
        raise NotImplementedError

    def test_transform(self):
        raise NotImplementedError

    def test_train(self):
        raise NotImplementedError

    def test_predict(self):
        raise NotImplementedError

    def test_evaluate(self):
        raise NotImplementedError

if __name__ == '__main__':
    unittest.main()
