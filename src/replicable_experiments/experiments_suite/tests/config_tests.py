import unittest

from experiments_suite.models.config_mongo import AlgorithmConfig, FeatureExtractionConfig, VectorizerConfig
from experiments_suite.utils import ClsType


class AlgorithmConfigTests(unittest.TestCase):
    def smoke_test(self):
        alg_config = AlgorithmConfig(alg_type=0)

        self.assertTrue(alg_config.alg_type == ClsType.LinearSvm)


class FeatureExtractionConfigTests(unittest.TestCase):
    def smoke_test(self):
        f_config = FeatureExtractionConfig(column_names=["a", "b"])

        self.assertTrue(f_config.column_names == ["a", "b"])


class VectorizerConfigTests(unittest.TestCase):
    def smoke_test(self):
        alg_config = VectorizerConfig()

        # self.assertTrue(alg_config.alg_type == ClsType.LinearSvm)


class TransformerConfigTests(unittest.TestCase):
    def smoke_test(self):
        alg_config = AlgorithmConfig(alg_type=0)

        self.assertTrue(alg_config.alg_type == ClsType.LinearSvm)


class DatasetConfigTests(unittest.TestCase):
    pass


class ExperimentConfigTests(unittest.TestCase):
    def smoke_test(self):
        alg_config = AlgorithmConfig(alg_type=0)

        self.assertTrue(alg_config.alg_type == ClsType.LinearSvm)


class ExperimentSuiteConfigTests(unittest.TestCase):
    def smoke_test(self):
        alg_config = AlgorithmConfig(alg_type=0)

        self.assertTrue(alg_config.alg_type == ClsType.LinearSvm)


if __name__ == "__main__":
    unittest.main()