import sklearn
import joblib
import numpy as np
from base_mongo import AlgorithmAbstractBase
from snippets.python_snippets import get_random_string
from utils import ClsType


class ScikitCls(AlgorithmAbstractBase):

    def __init__(self, config):
        super(ScikitCls, self).__init__()
        self.config = config
        self.cls = self.__initialize_cls(self.config.alg_type, self.config.alg_args, self.config.alg_kwargs)
        self.predictions = []
        self.__have_trees = [ClsType.DecisionTree, ClsType.RandomForestRegressor, ClsType.IsolationForest]
        self.__have_coeffs = [ClsType.SVR, ClsType.LinearSvm, ClsType.OneClassSvm]

    def __initialize_cls(self, cls_type, cls_args=None, cls_kwargs=None):
        # todo: use a dictionary to improve this
        cls_args = cls_args if cls_args is not None else ()
        cls_kwargs = cls_kwargs if cls_kwargs is not None else {}

        if cls_type == ClsType.MultinomialNB:
            from sklearn.naive_bayes import MultinomialNB
            return MultinomialNB(*cls_args, **cls_kwargs)
        elif cls_type == ClsType.Svm:
            from sklearn.multiclass import OneVsRestClassifier
            estimator = self.config.estimator_type if self.config.estimator_type is not None else ClsType.LinearSvm
            return OneVsRestClassifier(estimator=self.__initialize_cls(estimator))
        elif cls_type == ClsType.DecisionTree:
            from sklearn.tree import DecisionTreeClassifier
            return DecisionTreeClassifier(*cls_args, **cls_kwargs)
        elif cls_type == ClsType.LinearSvm:
            from sklearn.svm import LinearSVC
            return LinearSVC(*cls_args, **cls_kwargs)
        elif cls_type == ClsType.SVC:
            """
            Implementation of Support Vector Machine classifier using libsvm:
            the kernel can be non-linear but its SMO algorithm does not scale
            to large number of samples as LinearSVC does. Furthermore SVC multi-class mode
            is implemented using one vs one scheme while LinearSVC uses one vs the rest.
            It is possible to implement one vs the rest with SVC by using the
            sklearn.multiclass.OneVsRestClassifier wrapper. Finally SVC can fit dense data without memory copy
            if the input is C-contiguous. Sparse data will still incur memory copy though.
            """
            return sklearn.svm.SVC(*cls_args, **cls_kwargs)
        elif cls_type == ClsType.NuSVC:
            return sklearn.svm.NuSVC(*cls_args, **cls_kwargs)
        elif cls_type == ClsType.IsolationForest:
            return sklearn.ensemble.IsolationForest(*cls_args, **cls_kwargs)
        elif cls_type == ClsType.OneClassSvm:
            return sklearn.svm.OneClassSvm(*cls_args, **cls_kwargs)
        elif cls_type == ClsType.SVR:
            return sklearn.svm.SVR(*cls_args, **cls_kwargs)
        elif cls_type == ClsType.NuSVR:
            return sklearn.svm.NuSVR(*cls_args, **cls_kwargs)
        elif cls_type == ClsType.RandomForestRegressor:
            from sklearn.ensemble import RandomForestRegressor
            return RandomForestRegressor(n_estimators=2, max_depth=50)
        elif cls_type == ClsType.SGDClassifier:
            # loss = Defaults to 'hinge', which gives a linear SVM.
            # The 'log' loss gives logistic regression, a probabilistic classifier.
            from sklearn.linear_model import SGDClassifier
            return SGDClassifier(*cls_args, **cls_kwargs)
        elif cls_type == ClsType.Perceptron:
            from sklearn.linear_model import Perceptron
            return Perceptron(*cls_args, **cls_kwargs)
        elif cls_type == ClsType.AdaBoost:
            from sklearn.ensemble import AdaBoostClassifier
            return AdaBoostClassifier(base_estimator=self.__initialize_cls(ClsType.SGD), *cls_args, **cls_kwargs)
        elif cls_type == ClsType.Bagging:
            from sklearn.ensemble import BaggingClassifier
            from sklearn.linear_model import SGDClassifier
            return BaggingClassifier(base_estimator=SGDClassifier(loss='hinge'), *cls_args, **cls_kwargs)
        elif cls_type == ClsType.KMeans:
            from sklearn.cluster import KMeans
            return KMeans(*cls_args, **cls_kwargs)

        else:
            raise NotImplementedError("The type of classifier: {0} is not implemented".format(cls_type))

    def train(self, train_data, train_y=None):
        self.cls.fit(train_data, train_y)

    def predict(self, x_test):
        self.predictions = self.cls.predict(x_test)
        return self.predictions

    def get_feature_ranking(self, feature_columns, top=20):
        """
        Return the top most important features - depending on the classifier's implementation
        :param top: int the number of features to return
        :return:
        """
        # TODO: IMPLEMENT THIS ELSEWHERE - FeatureRanking
        from sklearn.externals.joblib import Parallel
        from sklearn.externals.joblib import delayed

        features_ranking = {}
        feature_importances = []
        indices = []

        if self.config.cls_type in self.__have_trees:
            if self.config.cls_type == ClsType.DecisionTree:
                feature_importances = self.cls.feature_importances_
            else:
                all_importances = Parallel(n_jobs=self.cls.n_jobs,
                                       backend="threading")\
                (delayed(getattr)(tree, 'feature_importances_') for tree in self.cls.estimators_)

                feature_importances = sum(all_importances) / len(self.cls.estimators_)

                std = np.std([tree.feature_importances_ for tree in self.cls.estimators_], axis=0)

        elif self.config.cls_type == ClsType.OneClassSvm:
            # TODO: fixme: not correct
            feature_importances = sum([imp for imp in self.cls.support_vectors_]) / len(self.cls.support_vectors_)

        indices = np.argsort(feature_importances)[::-1]
        if self.config.verbose:
            print("Feature ranking:")

            for f in xrange(top):
                if f < 10:
                    print("%d. feature %d (%f) %s" % \
                          (f + 1,
                           indices[f],
                           feature_importances[indices[f]],
                           feature_columns[indices[f]]))
                if len(features_ranking) == len(indices):
                    break
                features_ranking[f + 1] = {
                    "index": indices[f],
                    "coefficient": feature_importances[indices[f]],
                    "feature": feature_columns[indices[f]]
                }

        return features_ranking

    def save_model(self, path="", custom_name=""):
        """
          Saves current trained classifier's model to file
          classifier_name_datetime.pkl
          :return:None
        """
        joblib.dump(self.cls, '../data/cls_models/{0}.pkl'.format(str(self.config.cls_type) + custom_name))

    def load_model(self, path="", custom_name=""):
        import os.path
        full_path_cls = "../data/cls_models/{0}.pkl".format(self.config.cls_type + custom_name)

        if os.path.isfile(full_path_cls):
            self.cls = joblib.load(full_path_cls)
        else:
            raise Exception("Could not instantiate Classifier - File not found!")


class MaxVoteCls(AlgorithmAbstractBase):
    """
        Takes as input a list of pre-trained classifiers and calculates the Frequency Distribution of their predictions
    """

    def __init__(self, classifiers, weights=None):
        super(MaxVoteCls, self).__init__()
        self._classifiers = classifiers
        self.predictions = None
        self.verdicts = None
        self.weights = weights
        self._feature_rankings = None

    def _get_weighted_prediction(self, predictions):
        return 1 if np.average(np.array([predictions], dtype=float), axis=1, weights=self.weights) > 0 else -1

    def _get_prediction(self, predictions):
        from nltk import FreqDist
        counts = FreqDist()
        for i in xrange(0, len(predictions)):
            counts[predictions[i][j]] += 1
        return counts.max()

    def train(self, x_train_per_cls, y=None):
        if len(x_train_per_cls) != len(self._classifiers):
            raise Exception("Invalid length of x_test_per_cls: each classifier must have a corresponding x_test")
        for n, classifier in enumerate(self._classifiers):
            self._classifiers[n] = classifier.fit(x_train_per_cls[n], y=y)

    def predict(self, x_test):
        self.predictions = []
        self.verdicts = []

        # get all predictions
        for classifier in self._classifiers:
            self.predictions.append(classifier.predict(x_test))

        for j in xrange(0, len(x_test)):
            if self.weights:
                self.verdicts.append(self._get_weighted_prediction([self.predictions[i][j] for i in range(0, len(self.predictions))]))
            else:
                self.verdicts.append(self._get_prediction([self.predictions[i][j] for i in range(0, len(self.predictions))]))
        return self.verdicts

    def get_feature_ranking(self, column_names, top=20):
        feature_ranking = {}
        for cls in self._classifiers:
            name = type(cls).__name__
            if name in feature_ranking:
                name += get_random_string(3)
            feature_ranking[name] = cls.get_feature_ranking(column_names, top=top)
        return feature_ranking

    # def train(self, x_train, y_train=None):
    #     for cls in self._classifiers:
    #         cls.fit(x_train, y_train)

    def save_model(self, path="", custom_name=""):
        """
           Saves current trained classifier's model to file
           classifier_name_datetime.pkl
           :return:None
        """
        joblib.dump(self._classifiers, '../data/cls_models/{0}_MAX_VOTE.pkl'.format(
            self.config.cls_type + custom_name))

    def load_model(self, path="", custom_name=""):
        # TODO: might we need to save the whole object not just the classifiers?
        import os.path
        full_path_cls = "../data/cls_models/{0}.pkl".format(
            self.config.cls_type + custom_name)

        if os.path.isfile(full_path_cls):
            self._classifiers = joblib.load(full_path_cls)
        else:
            raise Exception("Could not instantiate Classifier - File not found!")


# def save_model(mapper, connect, target):
#     target.save_model()
#
# listen(ScikitCls, 'before_insert', save_model)