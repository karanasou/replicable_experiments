from base_mongo import CaseBase


class DataSetCase(CaseBase):
    def __init__(self, configs, datasets):
        super(DataSetCase, self).__init__(configs=configs)
        self.datasets = datasets


class AlgorithmCase(CaseBase):
    def __init__(self, configs):
        super(AlgorithmCase, self).__init__(configs=configs)


class FeaturesCase(CaseBase):
    def __init__(self, configs):
        super(FeaturesCase, self).__init__(configs=configs)


class TransformationCase(CaseBase):
    def __init__(self, configs):
        super(TransformationCase, self).__init__(configs=configs)


class VectorizationCase(CaseBase):
    def __init__(self, configs):
        super(VectorizationCase, self).__init__(configs=configs)


class AlgorithmParametersCase(CaseBase):
    def __init__(self):
        super(AlgorithmParametersCase, self).__init__()


class GridSearchCase(CaseBase):
    def __init__(self):
        super(GridSearchCase, self).__init__()