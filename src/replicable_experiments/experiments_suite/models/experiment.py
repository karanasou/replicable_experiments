from pymodm import fields
from experiments_suite.models.dataset import DataSet
from feature_extraction import ScikitVectorizer, ScikitTransformer
from algorithm import ScikitCls
from base_mongo import ExperimentBase
from experiments_suite.utils.enums import Step


class Experiment(ExperimentBase):
    """
    An Experiment represents the full cycle a ML test can follow.
    From the set up of the parameters to the vectorization, train, prediction and evaluation of the results.
    """
    def __init__(self, config=None, dataset=None):
        super(Experiment, self).__init__()
        self.config = config
        self.clf = None
        self.vectorizer = None
        self.transformer = None
        self.dataset = dataset
        self.predictions = None
        self.metrics = None
        self.step_to_action = {}
        self.__set_up()

    def __set_up(self):
        if self.config:
            if self.config.alg_config:
                self.clf = ScikitCls(self.config.alg_config)
            if self.config.vec_config:
                self.vectorizer = ScikitVectorizer(self.config.vec_config)
            if self.config.transformer_config:
                self.transformer = ScikitTransformer(self.config.transformer_config)
            # self.dataset = self.config.ds_config

        # initialize steps to actions
        self.step_to_action[Step.FeatureExtraction] = self.vectorize
        self.step_to_action[Step.Vectorization] = self.vectorize
        self.step_to_action[Step.Transformation] = self.transform
        self.step_to_action[Step.Train] = self.train
        self.step_to_action[Step.Predict] = self.predict
        self.step_to_action[Step.Evaluate] = self.evaluate
        self.step_to_action[Step.Save] = self.save

    def orchestrate(self, steps):
        self.dataset.split(self.config.ds_config.split)
        for step in steps:
            self.step_to_action[step]()

    def vectorize(self):
        self.vectorizer.vectorize()

    def transform(self):
        self.transformer.transform()

    def train(self):
        cls = self.config.ds_config.class_col
        self.clf.train(self.dataset.x_train.drop([cls], axis=1), self.dataset.x_train[cls])

    def predict(self):
        self.dataset.x_test["prediction"] = self.clf.predict(self.dataset.x_test.drop([self.config.ds_config.class_col], axis=1))

    def evaluate(self):
        from metrics import BasicMetrics
        self.metrics = BasicMetrics(self.dataset.x_test["prediction"].values.tolist(), self.dataset.x_test["class"].tolist())
        self.metrics.get_metrics()

    # def save(self):
        # session.add(self.vectorizer)
        # session.add(self.transformer)
        # session.add(self.clf)
        # session.commit()

    def __iter__(self):
        # [a for a in dir(obj) if not a.startswith('__') and not callable(getattr(obj, a))]
        yield "config", dict(self.config)
        yield "metrics", dict(self.metrics)