from __future__ import print_function
from __future__ import print_function
import math
from itertools import izip
import sklearn
from pymodm import fields
from sklearn.metrics import classification_report
from sklearn.utils.multiclass import unique_labels
from sklearn.metrics import mean_squared_error, f1_score, recall_score, precision_score, accuracy_score

from base_mongo import MetricsBase


__author__ = 'maria'


class CosineScorer(object):
    """
    Calculates the cosine similarity between two 1d lists, pair-wise.
    """
    def __init__(self, ground_truth, predictions):
        self.ground_truth = ground_truth
        self.predictions = predictions
        self._cosine = None
        if len(self.predictions) != len(self.ground_truth) or len(self.predictions) == 0:
            raise Exception("Predicted and actual lists are not valid")

    @property
    def cosine(self):
        if not self._cosine:
            sum_prod = 0
            mod1 = 0
            mod2 = 0
            for i in xrange(len(self.ground_truth)):
                sum_prod += self.ground_truth[i] * self.predictions[i]
                mod1 += self.ground_truth[i] * self.ground_truth[i]
                mod2 += self.predictions[i] * self.predictions[i]
            mod1 = math.sqrt(mod1)
            mod2 = math.sqrt(mod2)

            self._cosine = sum_prod / (mod1 * mod2)
        return self._cosine


class BasicMetrics(MetricsBase):
    """
    BasicMeasures has all the methods required to retrieve and save the following metrics:
    Accuracy, Precision, Recall, F-Score, Cosine Similarity, MSE
    """
    accuracy = fields.FloatField(blank=True)
    precision = fields.FloatField(blank=True)
    recall = fields.FloatField(blank=True)
    f_score = fields.FloatField(blank=True)
    cosine_similarity = fields.FloatField(blank=True)
    mse = fields.FloatField(blank=True)
    classification_report = fields.CharField(blank=True)
    classification_report_as_list = fields.ListField(blank=True)

    def __init__(self, predictions=(), ground_truth=()):
        super(BasicMetrics, self).__init__()
        self.predictions = predictions
        self.ground_truth = ground_truth
        if len(self.predictions) != len(self.ground_truth) or len(self.predictions) == 0:
            raise Exception("Predicted and actual lists are not valid")
        self.accuracy = 0.0
        self.precision = 0.0
        self.recall = 0.0
        self.f_score = 0.0
        self.cosine_similarity = 0.0
        self.mse = 0.0
        self.classification_report = 0.0
        self.classification_report_as_list = []

    def get_metrics(self):
        self.accuracy = self.get_accuracy()
        self.precision = self.get_precision()
        self.recall = self.get_recall()
        self.f_score = self.get_f_measure()
        self.cosine_similarity = self.get_cosine_similarity()
        self.mse = self.get_mse()
        self.classification_report = self.get_classification_report_str()
        self.classification_report_as_list = self.get_classification_report_as_list()

    def get_accuracy(self):
        return accuracy_score(self.ground_truth, self.predictions)

    def get_precision(self):
        return precision_score(self.ground_truth, self.predictions, average="weighted")

    def get_recall(self):
        return recall_score(self.ground_truth, self.predictions, average="weighted")

    def get_f_measure(self):
        return f1_score(self.ground_truth, self.predictions, average="weighted")

    def get_mse(self):
        return mean_squared_error(self.ground_truth, self.predictions)

    def get_cosine_similarity(self):
        return CosineScorer(self.ground_truth, self.predictions).cosine

    # todo: refactor the methods for different outputs of classification report
    def get_classification_report_str(self):
        return classification_report(self.ground_truth, self.predictions)

    def get_classification_report_as_list(self):
        labels = unique_labels(self.ground_truth, self.predictions)
        precision, recall, f_score, support = sklearn.metrics.precision_recall_fscore_support(self.ground_truth,
                                                                                              self.predictions,
                                                                                              labels=labels,
                                                                                              average=None)

        self.classification_report_as_list = izip(labels, precision, recall, f_score, support)

        return self.classification_report_as_list

    def ger_classification_report_as_dict(self):
        classif_report_dict = {}
        labels = unique_labels(self.ground_truth, self.predictions)

        precision, recall, f_score, support = sklearn.metrics.precision_recall_fscore_support(self.ground_truth,
                                                                                              self.predictions,
                                                                                              labels=labels,
                                                                                              average=None)

        for i in xrange(0, len(labels)):
            classif_report_dict[labels[i]] = {"precision": round(precision[i], 3),
                                              "recall": round(recall[i], 3),
                                              "f_score": round(f_score[i], 3),
                                              "support": support[i]
                                              }
        return classif_report_dict

    @staticmethod
    def classification_report_to_csv_pandas_way(ground_truth,
                                                predictions,
                                                full_path="test_pandas.csv"):
        """
        Saves the classification report to csv using the pandas module.
        :param ground_truth: list: the true labels
        :param predictions: list: the predicted labels
        :param full_path: string: the path to the file.csv where results will be saved
        :return: None
        """
        import pandas as pd

        # get unique labels / classes
        # - assuming all labels are in the sample at least once
        labels = unique_labels(ground_truth, predictions)

        # get results
        precision, recall, f_score, support = sklearn.metrics.precision_recall_fscore_support(ground_truth,
                                                                                              predictions,
                                                                                              labels=labels,
                                                                                              average=None)
        # a pandas way:
        results_pd = pd.DataFrame({"class": labels,
                                   "precision": precision,
                                   "recall": recall,
                                   "f_score": f_score,
                                   "support": support
                                   })

        results_pd.to_csv(full_path, index=False)

    @staticmethod
    def classification_report_to_csv(ground_truth,
                                     predictions,
                                     full_path="test_simple.csv"):
        """
        Saves the classification report to csv.
        :param ground_truth: list: the true labels
        :param predictions: list: the predicted labels
        :param full_path: string: the path to the file.csv where results will be saved
        :return: None
        """
        # get unique labels / classes
        # - assuming all labels are in the sample at least once
        labels = unique_labels(ground_truth, predictions)

        # get results
        precision, recall, f_score, support = sklearn.metrics.precision_recall_fscore_support(ground_truth,
                                                                                              predictions,
                                                                                              labels=labels,
                                                                                              average=None)

        # a non-pandas way:
        with open(full_path) as fp:
            for line in izip(labels, precision, recall, f_score, support):
                fp.write(",".join(line))

    def __iter__(self):
        yield "accuracy", self.accuracy
        yield "precision", self.precision
        yield "recall", self.recall
        yield "f_score", self.f_score
        yield "cosine_similarity", self.cosine_similarity
        yield "mse", self.mse
        yield "classification_report", self.classification_report
        yield "classification_report_as_list", self.classification_report_as_list


class CustomMetrics(MetricsBase):
    """
    Metrics for a specific case
    """
    def __init__(self, data_frame,
                 predictions_col="prediction",
                 ground_truth_col="class",
                 normal=1,
                 malicious=-1,
                 title="Metrics:"):
        super(CustomMetrics, self).__init__()
        self.normal_class = normal
        self.malicious_class = malicious
        self.predictions_col = predictions_col
        self.ground_truth_col = ground_truth_col
        self.title = title
        self.tp = 0
        self.tn = 0
        self.fp = 0
        self.fn = 0
        self.acc_total = 0.0
        self.acc_malicious = 0.0
        self.acc_normal = 0.0
        self.hits_total = 0
        self.hits_normal = 0
        self.hits_malicious = 0
        self.miss_total = 0
        self.miss_malicious = 0
        self.f1_total = 0
        self.f1_malicious = 0
        self.f1_benign = 0
        self.precision_total = 0
        self.precision_malicious = 0
        self.precision_normal = 0
        self.recall_total = 0
        self.recall_malicious = 0
        self.recall_normal = 0
        self.classification_report = []
        self.basic_measures = BasicMetrics(data_frame[self.ground_truth_col].values,
                                           data_frame[self.predictions_col].values)

        self._calculate(data_frame)

    def to_dict(self):
        return {
            "normal_clas": self.normal_class,
            "malicious_class": self.malicious_class,
            "tp": self.tp,
            "tn": self.tn,
            "fp": self.fp,
            "fn": self.fn,
            "acc_total": self.acc_total,
            "acc_malicious": self.acc_malicious,
            "acc_normal": self.acc_normal,
            "hits_total": self.hits_total,
            "hits_malicious": self.hits_malicious,
            "miss_total": self.miss_total,
            "miss_malicious": self.miss_malicious,
            "f1_total": self.f1_total,
            "f1_malicious": self.f1_malicious,
            "f1_benign": self.f1_benign,
            "precision_total": self.precision_total,
            "precision_malicious": self.precision_malicious,
            "precision_normal": self.precision_normal,
            "recall_total": self.recall_total,
            "recall_malicious": self.recall_malicious,
            "recall_normal": self.recall_normal,
            "classification_report": self.classification_report,
        }

    def _calculate(self, df):
        from utils.helpers import divide_or_zero

        results = df.groupby([self.ground_truth_col, self.predictions_col])
        resultsagg = results.size()
        print(resultsagg)

        self.acc_total = self.basic_measures.get_accuracy()
        self.precision_total = self.basic_measures.get_precision()
        self.recall_total = self.basic_measures.get_recall()
        self.classification_report = self.basic_measures.ger_classification_report_as_dict()

        self.acc_malicious = ((df[(df[self.ground_truth_col] == self.malicious_class) &
                                  (df[self.predictions_col] == self.malicious_class)].values.shape[0])
                              / float(df[df[self.ground_truth_col] == self.malicious_class].values.shape[0])) * 100
        self.acc_normal = ((df[(df[self.ground_truth_col] == self.normal_class) &
                               (df[self.predictions_col] == self.normal_class)].values.shape[0])
                           / float(df[df[self.ground_truth_col] == self.normal_class].values.shape[0])) * 100

        print(self.basic_measures.get_classification_report_str())

        # Malicious is considered Positive, Benign is considered Negative
        #                         [ actual,                         predicted ]
        self.tp = float(resultsagg[self.malicious_class, self.malicious_class]) if (self.malicious_class,
                                                                                    self.malicious_class) \
                                                                                   in resultsagg.index else 0
        #                         [ actual,                   predicted ]
        self.tn = float(resultsagg[self.normal_class, self.normal_class]) if (self.normal_class,
                                                                              self.normal_class) \
                                                                             in resultsagg.index else 0
        #                         [ actual,                     predicted ]
        self.fp = float(resultsagg[self.normal_class, self.malicious_class]) if (self.normal_class,
                                                                                 self.malicious_class) \
                                                                                in resultsagg.index else 0
        #                         [ actual,                     predicted ]
        self.fn = float(resultsagg[self.malicious_class, self.normal_class]) if (self.malicious_class,
                                                                                 self.normal_class) \
                                                                                in resultsagg.index else 0
        self.f1_malicious = 2 * self.tp / (2 * self.tp + self.fp + self.fn)
        self.f1_benign = 2 * self.tn / (2 * self.tn + self.fp + self.fn)  # bug: tp instead of tn

        self.hits_total = self.tp + self.tn
        self.hits_normal = self.tn
        self.hits_malicious = self.tp
        self.miss_total = self.fp + self.fn
        self.miss_normal = self.fn
        self.miss_malicious = self.fn
        self.total = self.hits_total + self.miss_total
        self.total_normal = df[df[self.ground_truth_col] == self.normal_class].count()
        self.total_malicious = df[df[self.ground_truth_col] == self.malicious_class].count()
        self.total_percentage = (float(self.hits_total) / self.total) * 100

        print("outlier distinction accuracy:", ((df[(df[self.ground_truth_col] == self.malicious_class)
                                                    & (df[self.predictions_col] == self.malicious_class)
                                                    ].values.shape[0])
                                                / float(
            df[df['class'] == self.malicious_class].values.shape[0])) * 100)

        self.acc_malicious = (self.tp / float(
            df[df[self.ground_truth_col] == self.malicious_class].values.shape[0])) * 100
        self.acc_normal = (self.tn / float(df[df[self.ground_truth_col] == self.normal_class].values.shape[0])) * 100

        self.precision_malicious = divide_or_zero(self.tp, (self.tp + self.fp))
        self.precision_normal = divide_or_zero(self.tn, (self.tn + self.fn))

        self.recall_malicious = divide_or_zero(self.tp, (self.tp + self.fn))
        self.recall_normal = divide_or_zero(self.tn, (self.tn + self.fp))
