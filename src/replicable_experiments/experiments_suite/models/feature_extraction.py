from base_mongo import VectorizerAbstractBase, TransformerAbstractBase, DatasetProcessorAbstractBase, FeatureExtractionBase
from experiments_suite.utils.enums import VecType


class DatasetProcessor(DatasetProcessorAbstractBase):

    def __init__(self, config):
        super(DatasetProcessor, self).__init__()
        self.config = config

    def set_data(self, *args, **kwargs):
        pass

    def process(self, *args, **kwargs):
        pass

    def save(self):
        pass


class FeatureExtraction(FeatureExtractionBase):

    def __init__(self, config):
        super(FeatureExtraction, self).__init__()
        self.config = config


class ScikitVectorizer(VectorizerAbstractBase):

    def __init__(self, config):
        super(ScikitVectorizer, self).__init__()
        self.config = config
        self.vectorizer = self.__initialize(self.config.vec_type)

    def __set_up(self):
        pass

    def __initialize(self, vec_type):
        if vec_type == VecType.Dict:
            from sklearn.feature_extraction import DictVectorizer
            return DictVectorizer(sparse=True)
        elif vec_type == VecType.Count:
            from sklearn.feature_extraction.text import CountVectorizer
            return CountVectorizer(analyzer='word',
                                   input='content',
                                   lowercase=True,
                                   min_df=1,
                                   binary=False,
                                   stop_words='english')
        elif vec_type == VecType.TfIdf:
            from sklearn.feature_extraction.text import TfidfVectorizer
            return TfidfVectorizer(use_idf=True, stop_words='english')
        elif vec_type == VecType.Tf:
            from sklearn.feature_extraction.text import TfidfVectorizer
            return TfidfVectorizer(use_idf=False, stop_words='english')
        else:
            raise NotImplementedError("Vectorizer of type {0} is not yet implemented.".format(vec_type))

    def set_data(self, *args, **kwargs):
        pass

    def vectorize(self, *args, **kwargs):
        pass

    def transform(self):
        pass

    def save(self):
        pass


class ScikitTransformer(TransformerAbstractBase):

    def __init__(self, config):
        super(ScikitTransformer, self).__init__()
        self.config = config

    def transform(self, *args, **kwargs):
        pass

    def set_data(self, *args, **kwargs):
        pass

    def save(self):
        pass

