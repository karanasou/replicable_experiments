class Formatter(object):

    def accept(self, visitor, *args, **kwargs):
        raise NotImplementedError("You used the base class' method which is not implemented!")


class HtmlFormatter(Formatter):
    def accept(self, visitor):
        visitor.visit(self)


class CsvFormatter(Formatter):
    def accept(self, visitor):
        visitor.visit(self)


class ExperimentReport(object):
    """
    Create a report with the experiment's results
    """
    def __init__(self, experiment):
        self.experiment = experiment

    def report(self):
        import pandas as pd

        d = dict(self.experiment)

        metrics = pd.DataFrame(dict(d["metrics"]))
        cfg = pd.DataFrame({key: [value] for key, value in d["config"].items()})
        return metrics, cfg


class ExperimentSuiteReport(object):
    """
    Create a report about which experiments run so far, which didn't and about the results.
    """
    def __init__(self, experiment_suite):
        self.experiment_suite = experiment_suite

    def report(self):
        # todo: incomplete - use with formatters
        import pandas as pd
        metric_results = []
        config_results = []
        for exp in self.experiment_suite.experiments:
            metrics, config = ExperimentReport(exp).report()
            metric_results.append(metrics)
            config_results.append(config)

        report = pd.concat(metric_results, ignore_index=True)
        best = max(report["accuracy"])
        print("Best Accuracy:", best)
        best_index = report[report["accuracy"]==best].index
        report_cfg = pd.concat(config_results, ignore_index=True)
        print("Best Configuration:", best_index)
        print(report_cfg.ix[best_index])
        print(report.to_string())

        print report_cfg.to_string()