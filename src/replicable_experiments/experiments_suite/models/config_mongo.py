import traceback
from itertools import izip

from pymodm import fields
from experiments_suite.snippets import limit_exec_ram
from experiments_suite.utils.enums import ClsType, VecType
from exception import InvalidConfiguration
from base_mongo import ConfigBase, EmbeddedConfigBase


class AlgorithmConfig(EmbeddedConfigBase):
    __tablename__ = 'alg_config'

    alg_type = fields.IntegerField()
    alg_args = fields.ListField(blank=True)
    alg_kwargs = fields.DictField(blank=True)

    def __init__(self, alg_type=ClsType.LinearSvm, alg_args=None, alg_kwargs=None):
        super(AlgorithmConfig, self).__init__()
        self.alg_type = int(alg_type)
        self.alg_args = alg_args
        self.alg_kwargs = alg_kwargs
        self._sanity_check()

    def _sanity_check(self):
        pass
        # if not isinstance(self.alg_type, ClsType):
        #     raise InvalidConfiguration("Wrong type of algorithm:{}".format(type(self.alg_type)))

    def set_defaults(self, *args, **kwargs):
        pass

    def __iter__(self):
        yield "alg_type", self.alg_type
        yield "alg_args", self.alg_args
        yield "alg_kwargs", self.alg_kwargs


class FeatureExtractionConfig(EmbeddedConfigBase):
    # experiment_config_id = Column(Integer, ForeignKey("experiment_config.id", ondelete='CASCADE'))
    column_names = fields.ListField()
    method = fields.Code(code="python")
    # dataset = relationship("FeatureExtraction", uselist=False, back_populates="feature_extraction_config")
    # feature_extraction = relationship("FeatureExtraction", uselist=False, back_populates="config")

    def __init__(self, column_names=(), method=VecType.Count):
        super(FeatureExtractionConfig, self).__init__()
        self.column_names = column_names
        self.method = method

    def set_defaults(self, *args, **kwargs):
        pass

    def __iter__(self):
        yield "column_names", self.column_names
        yield "method", int(self.method)


class TransformerConfig(EmbeddedConfigBase):
    method = fields.IntegerField()

    def __init__(self, vec_type=VecType.Count):
        super(TransformerConfig, self).__init__()
        self.vec_type = vec_type

    def __iter__(self):
        yield "vec_type", int(self.vec_type)

    def _sanity_check(self, *args, **kwargs):
        pass

    def set_defaults(self, *args, **kwargs):
        pass


class VectorizerConfig(EmbeddedConfigBase):
    vec_type = fields.IntegerField()

    def __init__(self, vec_type=VecType.Count):
        super(VectorizerConfig, self).__init__()
        self.vec_type = vec_type

    def __iter__(self):
        yield "vec_type", int(self.vec_type)

    def _sanity_check(self, *args, **kwargs):
        pass

    def set_defaults(self, *args, **kwargs):
        pass


class DatasetConfig(EmbeddedConfigBase):
    class_col = fields.CharField()
    str_split = fields.CharField(blank=True)
    shuffle = fields.BooleanField()
    cv = fields.IntegerField(blank=True)

    def __init__(self, class_col="class", split=0.8, shuffle=True, cv=-1):
        super(DatasetConfig, self).__init__()
        self.class_col = class_col
        self._split = None
        self.split = split
        self.str_split = self.__stringify_split()
        self.shuffle = shuffle
        self.cv = cv
        self.__initialize()

    @property
    def split(self):
        return self._split

    @split.getter
    def split(self):
        if self._split is None and self.str_split is not None:
            if isinstance(self.str_split, str):
                try:
                    self._split = float(self.str_split)
                except ValueError:
                    try:
                        self._split = compile(self.str_split)
                    except Exception:
                        traceback.print_exc()
        return self._split

    @split.setter
    def split(self, value):
        if isinstance(value, str):
            try:
                self._split = float(value)
            except ValueError:
                try:
                    self._split = compile(value)
                except Exception:
                    traceback.print_exc()
        else:
            self._split = value

    def __initialize(self):
        pass

    def set_defaults(self, *args, **kwargs):
        pass

    def __iter__(self):
        yield "class_col", self.class_col
        yield "split", self.str_split
        yield "shuffle", self.shuffle
        yield "cv", self.cv

    def __stringify_split(self):
        if isinstance(self.split, float):
            return str(self.split)
        else:
            import inspect
            return inspect.getsource(self.split)


class ExperimentConfig(EmbeddedConfigBase):
    """
    Class to contain the configuration for each Experiment.
    """

    memory_limit = fields.IntegerField()
    created_on = fields.DateTimeField()
    alg_config = fields.EmbeddedDocumentField(AlgorithmConfig, blank=True)
    feature_extraction_config = fields.EmbeddedDocumentField(FeatureExtractionConfig, blank=True)
    vec_config = fields.EmbeddedDocumentField(VectorizerConfig, blank=True)
    transformer_config = fields.EmbeddedDocumentField(TransformerConfig, blank=True)
    ds_config = fields.EmbeddedDocumentField(DatasetConfig, blank=True)
    steps = fields.ListField(fields.CharField(), blank=True, default=[])

    def __init__(self,
                 ds_config=None,
                 alg_config=None,
                 feature_extraction_config=None,
                 vec_config=None,
                 transformer_config=None,
                 steps=None,
                 memory_limit=-1):
        super(ExperimentConfig, self).__init__()
        self.alg_config = alg_config
        self.feature_extraction_config = feature_extraction_config  # TODO: feature extraction vs vec?
        self.vec_config = vec_config
        self.transformer_config = transformer_config
        self.ds_config = ds_config
        self.memory_limit = memory_limit
        self.steps = steps

        if self.memory_limit > 0:
            limit_exec_ram(self.memory_limit)

    def _sanity_check(self, *args, **kwargs):
        pass

    def set_defaults(self, *args, **kwargs):
        """

        :param args:
        :param kwargs:
        :return:
        """
        raise NotImplementedError("todo....")

    def __iter__(self):
        """
        Override to facilitate ParameterGrid
        :return: tuple(string, config): Yields pairs of configuration names and the respective instances
        """
        fields = ['alg_config', 'feature_extraction_config', 'ds_config', 'vec_config', 'transformer_config']
        values = [self.alg_config, self.feature_extraction_config, self.ds_config, self.vec_config, self.transformer_config]
        i_values = [(x, dict(y)) for x, y in izip(fields, values) if y]
        for a, b in i_values:
            yield a, b
        # yield "alg_config", dict(self.alg_config)
        # yield "feature_extraction_config", dict(self.feature_extraction_config)
        # yield "vec_config", dict(self.vec_config)
        # yield "transformer_config", dict(self.transformer_config)
        # yield "ds_config", dict(self.ds_config)


class ExperimentSuiteConfig(ConfigBase):
    """

    """
    alg_configs = fields.ListField(fields.EmbeddedDocumentField(AlgorithmConfig, blank=True), blank=True)
    feature_extraction_configs = fields.ListField(fields.EmbeddedDocumentField(FeatureExtractionConfig, blank=True), blank=True)
    ds_configs = fields.ListField(fields.EmbeddedDocumentField(DatasetConfig, blank=True), blank=True)
    vec_configs = fields.ListField(fields.EmbeddedDocumentField(VectorizerConfig, blank=True), blank=True)
    transformation_configs = fields.ListField(fields.EmbeddedDocumentField(TransformerConfig, blank=True), blank=True)
    resume = fields.BooleanField(default=False)
    auto_save = fields.BooleanField(default=True)
    parallelize = fields.BooleanField(default=False)

    def __init__(self, resume=False, auto_save=True, parallelize=True):
        super(ExperimentSuiteConfig, self).__init__()
        self.resume = resume
        self.auto_save = auto_save
        self.alg_configs = []
        self.feature_extraction_configs = []
        self.ds_configs = []
        self.transformation_configs = []
        self.parallelize = parallelize==True

    def __iter__(self):
        """
        Override to facilitate ParameterGrid
        :return: tuple(string, config): Yields pairs of configuration names and the respective instances
        """
        fields = ['alg_config', 'feature_extraction_config', 'ds_config', 'vec_config', 'transformer_config']
        values = [self.alg_configs, self.feature_extraction_configs, self.ds_configs, self.vec_configs, self.transformation_configs]

        i_values = [(x, y) for x, y in izip(fields, values) if y]

        for a, b in i_values:
            yield a, b
        # for pair in izip(fields, values):
        #     if pair[1]:
        #         yield pair
        # yield 'alg_config', self.alg_configs
        # yield 'feature_extraction_config', self.feature_extraction_configs
        # yield 'ds_config', self.ds_configs
        # yield 'vec_config', self.vec_configs
        # yield 'transformer_config', self.transformation_configs

    def set_defaults(self, *args, **kwargs):
        raise NotImplementedError("todo....")

