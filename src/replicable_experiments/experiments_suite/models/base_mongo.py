import sys
from os import path
from bson import ObjectId
from pymodm import EmbeddedMongoModel
from pymodm import MongoModel
from pymodm import fields
from experiments_suite.utils import get_proper_table_name
from abc import abstractmethod, ABCMeta

sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))


class ConfigBase(MongoModel):
    pk = fields.ObjectIdField(primary_key=True)

    def __init__(self, *args, **kwargs):
        super(ConfigBase, self).__init__(*args, **kwargs)
        self.pk = ObjectId()

    def __repr__(self):
        # fixme: this is because of an issue with the multiprocessing module and MongoModel
        return self.__class__.__name__

    @abstractmethod
    def _sanity_check(self, *args, **kwargs):
        pass

    @abstractmethod
    def set_defaults(self, *args, **kwargs):
        pass


class EmbeddedConfigBase(EmbeddedMongoModel):
    pk = fields.ObjectIdField(primary_key=True)

    def __init__(self, *args, **kwargs):
        super(EmbeddedConfigBase, self).__init__(*args, **kwargs)
        self.pk = ObjectId()

    @abstractmethod
    def _sanity_check(self, *args, **kwargs):
        pass

    @abstractmethod
    def set_defaults(self, *args, **kwargs):
        pass

    @abstractmethod
    def __iter__(self):
        pass


class FeatureRankingBase(MongoModel):
    pk = fields.ObjectIdField(primary_key=True)

    def __init__(self, *args, **kwargs):
        super(FeatureRankingBase, self).__init__(*args, **kwargs)
        self.pk = ObjectId()

    def __repr__(self):
        return self.__class__.__name__

    def __tablename__(cls):
        return get_proper_table_name(cls.__name__)


class MetricsBase(EmbeddedMongoModel):

    pk = fields.ObjectIdField()
    predictions = fields.ListField()
    ground_truth = fields.ListField()
    # experiment = fields.ReferenceField(ExperimentBase)

    def __init__(self, *args, **kwargs):
        super(MetricsBase, self).__init__(*args, **kwargs)
        self.pk = ObjectId()

    def __tablename__(cls):
        return get_proper_table_name(cls.__name__)


class DataSetBase(MongoModel):

    pk = fields.ObjectIdField(primary_key=True)

    def __init__(self, *args, **kwargs):
        super(DataSetBase, self).__init__(*args, **kwargs)
        self.pk = ObjectId()

    def __repr__(self):
        return self.__class__.__name__

    def __tablename__(cls):
        return get_proper_table_name(cls.__name__)


class ExperimentBase(MongoModel):
    __abstract__ = True

    pk = fields.ObjectIdField(primary_key=True)
    dataset = fields.ReferenceField(DataSetBase, blank=True)
    config = fields.EmbeddedDocumentField(EmbeddedConfigBase, blank=True)
    metrics = fields.EmbeddedDocumentField(MetricsBase, blank=True)

    def __init__(self, *args, **kwargs):
        super(ExperimentBase, self).__init__(*args, **kwargs)
        self.pk = ObjectId()

    def __repr__(self):
        return self.__class__.__name__

    def __tablename__(cls):
        return get_proper_table_name(cls.__name__)


class AlgorithmAbstractBase(object):
    __metaclass__ = ABCMeta
    # __abstract__ = True

    def __init__(self):
        pass

    @abstractmethod
    def train(self, *args, **kwargs):
        pass

    @abstractmethod
    def predict(self, *args, **kwargs):
        pass

    @abstractmethod
    def get_feature_ranking(self, *args, **kwargs):
        pass

    @abstractmethod
    def save_model(self, path="", custom_name=""):
        pass

    @abstractmethod
    def load_model(self, path="", custom_name=""):
        pass


class FeatureExtractionBase(object):
    __metaclass__ = ABCMeta

    def __init__(self):
        pass


class VectorizerAbstractBase(object):
    __metaclass__ = ABCMeta

    def __init__(self):
        pass

    @abstractmethod
    def set_data(self, *args, **kwargs):
        pass

    @abstractmethod
    def vectorize(self, *args, **kwargs):
        pass


class TransformerAbstractBase(object):
    __metaclass__ = ABCMeta

    def __init__(self):
        pass

    @abstractmethod
    def set_data(self, *args, **kwargs):
        pass

    @abstractmethod
    def transform(self, *args, **kwargs):
        pass


class DatasetProcessorAbstractBase(object):
    __metaclass__ = ABCMeta

    def __init__(self):
        pass

    @abstractmethod
    def set_data(self, *args, **kwargs):
        pass

    @abstractmethod
    def process(self, *args, **kwargs):
        pass


class CaseBase(MongoModel):

    pk = fields.ObjectIdField()

    def __init__(self, configs=()):
        super(CaseBase, self).__init__()
        self.pk = ObjectId()
        self.configs = configs

    def __tablename__(cls):
        return get_proper_table_name(cls.__name__)



