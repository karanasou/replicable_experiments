from pathos.multiprocessing import ProcessingPool as Pool
from bson import ObjectId
from datetime import datetime
import time
from pymodm import MongoModel
from pymodm import fields
from experiment import Experiment
from sklearn.model_selection import ParameterGrid
from experiments_suite.models.config_mongo import ExperimentSuiteConfig, ExperimentConfig
from experiments_suite.utils import Step


def _p_execute_experiment(args):
    g, resume, parameters, dataset = args
    p = {key: dict(value) for key, value in g.items()}
    if resume:
        if p in parameters:
            print "Skipping", p
            return
    e_cfg = ExperimentConfig(**g)
    e = Experiment(config=e_cfg, dataset=dataset)
    e.orchestrate([Step.Train, Step.Predict, Step.Evaluate])  # todo: where should this go?
    # fixme: if experiments are saved and the instance of suite is not we will be left with orphan experiments
    # - which is not that bad -
    # but also the reverse can happen so we will be left with experiment references but no experiments
    return e, p


class ExperimentSuite(MongoModel):
    pk = fields.ObjectIdField(primary_key=True)
    experiments = fields.ListField(fields.ReferenceField(Experiment, on_delete=fields.ReferenceField.CASCADE),
                                   blank=True, default=[])
    config = fields.EmbeddedDocumentField(ExperimentSuiteConfig, blank=True)
    parameters = fields.ListField(fields.DictField(), blank=True)
    date_created = fields.DateTimeField()
    last_updated = fields.DateTimeField()

    def __init__(self, config=None):
        super(ExperimentSuite, self).__init__()
        self.pk = ObjectId()
        self.config = config
        self.experiments = []
        self.parameters = []
        self.times = []
        self.parameter_grid = None
        self.date_created = datetime.utcnow()
        self.last_updated = datetime.utcnow()
        self.dataset = None
        # self._save = super(ExperimentSuite, self).save

    def clean_up(self):
        self.experiments = []
        self.parameters = []
        self.times = []

    def start(self, dataset):
        self.dataset = dataset
        if not self.config.resume:
            self.clean_up()
        self.last_updated = datetime.utcnow()
        self.parameter_grid = ParameterGrid(dict(self.config))
        if self.config.parallelize:
            self._parallel_experiment()
        else:
            for g in self.parameter_grid:
                if self._execute_experiment(g) is None:
                    continue

    def _execute_experiment(self, g):
        p = {key: dict(value) for key, value in g.items()}
        if self.config.resume:
            if p in self.parameters:
                print "Skipping", p
                return
        e_cfg = ExperimentConfig(**g)
        e = Experiment(config=e_cfg, dataset=self.dataset)
        e.orchestrate([Step.Train, Step.Predict, Step.Evaluate])  # todo: where should this go?
        # fixme: if experiments are saved and the instance of suite is not we will be left with orphan experiments
        # - which is not that bad -
        # but also the reverse can happen so we will be left with experiment references but no experiments
        self.experiments.append(e)
        self.parameters.append(p)
        if self.config.auto_save:
            e.save()
            self.save()

    def _parallel_experiment(self):
        p = Pool(4)
        ps = []
        for g in self.parameter_grid:
            ps.append((g, self.config.resume, self.parameters, self.dataset))

        results = p.amap(_p_execute_experiment, ps)
        # self.experiments = Parallel(n_jobs=2)(delayed(self._execute_experiment)(g for g in self.parameter_grid))
        while not results.ready():  # TODO: check pool for failures and exit while loop
            print("num left: {}".format(results._number_left))
            # time.sleep(results._number_left/len(ps)) # todo: dynamically increase/ decrease time sleep
            time.sleep(2)

        for e, p in results.get():
            self.experiments.append(e)
            self.parameters.append(p)
            e.save()
        self.save()

    def set_up(self):
        pass

    def tear_down(self):
        pass

        # def save(self, experiment=None, *args, **kwargs):
        #     experiment.save()
        #     # for e in self.experiments:
        #     #     e.save()
        #     self._save(*args, **kwargs)

