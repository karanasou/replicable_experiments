import sys
from os import path

from sqlalchemy.orm import relationship


sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

from abc import abstractmethod, ABCMeta
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy import Column, Integer, String
from experiments_suite.db.sqla_db import Base
from experiments_suite.utils import get_proper_table_name


class ConfigBase(Base):
    __abstract__ = True
    # __metaclass__ = ABCMeta

    id = Column(Integer, primary_key=True)

    def __init__(self):
        pass

    @declared_attr
    def __tablename__(cls):
        return get_proper_table_name(cls.__name__)

    @abstractmethod
    def __sanity_check(self, *args, **kwargs):
        pass

    @abstractmethod
    def set_defaults(self, *args, **kwargs):
        pass


class ExperimentBase(Base):
    __abstract__ = True

    id = Column(Integer, primary_key=True)

    def __init__(self):
        pass

    @declared_attr
    def __tablename__(cls):
        return get_proper_table_name(cls.__name__)


class FeatureRankingBase(Base):
    __abstract__ = True
    # __metaclass__ = ABCMeta

    id = Column(Integer, primary_key=True)

    def __init__(self):
        pass

    @declared_attr
    def __tablename__(cls):
        return get_proper_table_name(cls.__name__)


class MetricsBase(Base):
    __abstract__ = True
    # __metaclass__ = ABCMeta

    id = Column(Integer, primary_key=True)

    def __init__(self):
        pass

    @declared_attr
    def __tablename__(cls):
        return get_proper_table_name(cls.__name__)


class DataSetBase(Base):
    __abstract__ = True
    # __metaclass__ = ABCMeta

    id = Column(Integer, primary_key=True)

    def __init__(self):
        pass

    @declared_attr
    def __tablename__(cls):
        return get_proper_table_name(cls.__name__)


class AlgorithmAbstractBase(object):
    __metaclass__ = ABCMeta
    # __abstract__ = True
    # id = Column(Integer, primary_key=True)

    def __init__(self):
        pass

    # @declared_attr
    # def __tablename__(cls):
    #     return get_proper_table_name(cls.__name__)

    @abstractmethod
    def train(self, *args, **kwargs):
        pass

    @abstractmethod
    def predict(self, *args, **kwargs):
        pass

    @abstractmethod
    def get_feature_ranking(self, *args, **kwargs):
        pass

    @abstractmethod
    def save_model(self, path="", custom_name=""):
        pass

    @abstractmethod
    def load_model(self, path="", custom_name=""):
        pass


class FeatureExtractionBase(object):
    # __abstract__ = True
    __metaclass__ = ABCMeta

    # id = Column(Integer, primary_key=True)

    def __init__(self):
        pass

    # @declared_attr
    # def __tablename__(cls):
    #     return get_proper_table_name(cls.__name__)


class VectorizerAbstractBase(object):

    # __abstract__ = True
    __metaclass__ = ABCMeta

    # id = Column(Integer, primary_key=True)

    def __init__(self):
        pass

    # @declared_attr
    # def __tablename__(cls):
    #     return get_proper_table_name(cls.__name__)

    @abstractmethod
    def set_data(self, *args, **kwargs):
        pass

    @abstractmethod
    def vectorize(self, *args, **kwargs):
        pass


class TransformerAbstractBase(object):
    # __abstract__ = True
    __metaclass__ = ABCMeta

    # id = Column(Integer, primary_key=True)

    def __init__(self):
        pass

    # @declared_attr
    # def __tablename__(cls):
    #     return get_proper_table_name(cls.__name__)

    @abstractmethod
    def set_data(self, *args, **kwargs):
        pass

    @abstractmethod
    def transform(self, *args, **kwargs):
        pass


class DatasetProcessorAbstractBase(object):

    # __abstract__ = True
    __metaclass__ = ABCMeta

    # id = Column(Integer, primary_key=True)

    def __init__(self):
        pass

    # @declared_attr
    # def __tablename__(cls):
    #     return get_proper_table_name(cls.__name__)

    @abstractmethod
    def set_data(self, *args, **kwargs):
        pass

    @abstractmethod
    def process(self, *args, **kwargs):
        pass


class CaseBase(Base):
    __abstract__ = True
    # __metaclass__ = ABCMeta

    id = Column(Integer, primary_key=True)

    def __init__(self):
        pass

    @declared_attr
    def __tablename__(cls):
        return get_proper_table_name(cls.__name__)



