from abc import ABCMeta

from sqlalchemy import Boolean
from sqlalchemy import Column, Integer, String
from sqlalchemy import ForeignKey
from sqlalchemy import PickleType
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.orm import relationship, backref

from base import ConfigBase
from utils import ClsType, VecType

from exception import InvalidConfiguration


class ExperimentConfig(ConfigBase):
    # alg_config_id = Column(Integer, ForeignKey("alg_config.id", ondelete='CASCADE'))
    # vec_config_id = Column(Integer, ForeignKey("vectorizer_config.id", ondelete='CASCADE'))
    # feature_extraction_config_id = Column(Integer, ForeignKey("feature_extraction_config.id", ondelete='CASCADE'))
    # transformer_config_id = Column(Integer, ForeignKey("transformer_config.id", ondelete='CASCADE'))
    # ds_config_id = Column(Integer, ForeignKey("dataset_config.id", ondelete='CASCADE'))
    memory_limit = Column(Integer)
    alg_config = relationship("AlgorithmConfig", uselist=False)
    feature_extraction_config = relationship("FeatureExtractionConfig", uselist=False)
    vec_config = relationship("VectorizerConfig", uselist=False)
    transformer_config = relationship("TransformerConfig", uselist=False)
    ds_config = relationship("DatasetConfig", uselist=False)

    def __init__(self, ds_config, alg_config, feature_extraction_config, vec_config, transformer_config, memory_limit=-1):
        super(ExperimentConfig, self).__init__()
        self.alg_config = alg_config
        self.feature_extraction_config = feature_extraction_config  # TODO: feature extraction vs vec?
        self.vec_config = vec_config
        self.transformer_config = transformer_config
        self.ds_config = ds_config
        self.memory_limit = memory_limit
        # TODO: check if memory_limit is valid? maybe hint how it should be given?
        if self.memory_limit > 0:
            import resource
            rsrc = resource.RLIMIT_AS
            soft, hard = resource.getrlimit(rsrc)
            print 'Soft limit starts as  :', soft, hard
            resource.setrlimit(rsrc, (self.memory_limit, hard))
            soft, hard = resource.getrlimit(rsrc)
            print 'Soft limit changed to :', soft, hard, "\n"

    def set_defaults(self, *args, **kwargs):
        pass


class ExperimentSuiteConfig(ConfigBase):
    __tablename__ = 'experiment_suite_config'

    def __init__(self):
        super(ExperimentSuiteConfig, self).__init__()

    def set_defaults(self, *args, **kwargs):
        pass


class AlgorithmConfig(ConfigBase):
    __tablename__ = 'alg_config'
    experiment_config_id = Column(Integer, ForeignKey("experiment_config.id", ondelete='CASCADE'))
    alg_type = Column(PickleType)
    alg_args = Column(PickleType)
    alg_kwargs = Column(PickleType)

    def __init__(self, alg_type, alg_args=None, alg_kwargs=None):
        super(AlgorithmConfig, self).__init__()
        self.alg_type = alg_type
        self.alg_args = alg_args
        self.alg_kwargs = alg_kwargs
        self.__sanity_check()

    def __sanity_check(self):
        if not isinstance(self.alg_type, ClsType):
            raise InvalidConfiguration("Wrong type of algorithm")

    def set_defaults(self, *args, **kwargs):
        pass


class FeatureExtractionConfig(ConfigBase):
    experiment_config_id = Column(Integer, ForeignKey("experiment_config.id", ondelete='CASCADE'))
    column_names = Column(PickleType)
    method = Column(PickleType)
    # dataset = relationship("FeatureExtraction", uselist=False, back_populates="feature_extraction_config")
    # feature_extraction = relationship("FeatureExtraction", uselist=False, back_populates="config")

    def __init__(self, column_names, method=VecType.Count):
        super(FeatureExtractionConfig, self).__init__()
        self.column_names = column_names
        self.method = method

    def set_defaults(self, *args, **kwargs):
        pass


class TransformerConfig(ConfigBase):
    experiment_config_id = Column(Integer, ForeignKey("experiment_config.id", ondelete='CASCADE'))

    def __init__(self):
        super(TransformerConfig, self).__init__()
        self.vec_type = VecType.Count

    def set_defaults(self, *args, **kwargs):
        pass


class VectorizerConfig(ConfigBase):
    experiment_config_id = Column(Integer, ForeignKey("experiment_config.id", ondelete='CASCADE'))

    def __init__(self):
        super(VectorizerConfig, self).__init__()
        self.method = VecType.Count

    def set_defaults(self, *args, **kwargs):
        pass


class DatasetConfig(ConfigBase):
    experiment_config_id = Column(Integer, ForeignKey("experiment_config.id", ondelete='CASCADE'))

    dataset_id = Column(Integer, ForeignKey('data_set.id'))
    dataset = relationship("DataSet", uselist=False, back_populates="config")
    split = Column(PickleType)
    shuffle = Column(Boolean)

    def __init__(self, class_col, split=0.8, shuffle=True):
        super(DatasetConfig, self).__init__()
        self.class_col = class_col
        self.split = split
        self.shuffle = shuffle
        self.__initialize()

    def __initialize(self):
        pass

    def set_defaults(self, *args, **kwargs):
        pass
