from pymodm import fields
from sqlalchemy import Column
from sqlalchemy import ForeignKey
from sqlalchemy import Integer
from sqlalchemy import PickleType
from sqlalchemy.orm import relationship

# from base import DataSetBase
from base_mongo import DataSetBase
# from config import DatasetConfig
from exception import InvalidConfiguration
import pandas as pd


class DataSet(DataSetBase):
    """
    One Dataset can have one configuration
    One configuration can belong to many datasets
    """
    # http://stackoverflow.com/a/1378818/3433323
    # df = Column(PickleType)
    df_as_list = fields.ListField()
    # config = fields.EmbeddedDocumentField(DataSetBase)
    # parent_id = Column(Integer, ForeignKey('dataset_config.id'))
    # config = relationship("DatasetConfig", back_populates="dataset")
    # parent = relationship("DatasetConfig", back_populates="dataset_config")

    def __init__(self, df=()):
        super(DataSet, self).__init__()
        self.df = df
        self.df_as_list = []
        self.x_train = []
        self.x_test = []
        self._sanity_check()

    def _sanity_check(self):
        if not isinstance(self.df, pd.DataFrame):
            raise InvalidConfiguration("`df` must be instance of pandas.DataFrame")
        self.df_as_list = self.df.values.tolist()
        # if not isinstance(self.config, DatasetConfig):
        #     raise InvalidConfiguration("`config` must be instance of DatasetConfig")

    def split(self, split):
        from types import FunctionType
        if isinstance(split, float):
            from sklearn.model_selection import train_test_split
            self.x_train, self.x_test, y_train, y_test = train_test_split(self.df,
                                                                          self.df["class"],
                                                                          test_size=1-split,
                                                                          random_state=42)
            # import numpy as np
            # self.df["is_train"] = np.random.uniform(0, 1) < split

        elif isinstance(split, FunctionType):
            # todo: should split change df or update x_train etc?
            self.x_train, self.x_test, y_train, y_test = split(self.df)

        else:
            raise Exception("Sth went wrong :(")
        self.x_test.is_copy = False
        # self.x_train = self.df[self.df["is_train"]==True]
        # self.x_test = self.df[self.df["is_train"]==False]
