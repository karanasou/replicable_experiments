def get_random_string(n=3, include_digits=True, include_characters=True):
    from random import random
    import string

    if not include_characters and not include_digits:
        raise Exception("Must include at least one of (digits, characters)")

    values = ''

    if include_characters:
        values = string.ascii_uppercase
    if include_digits:
        values += string.digits

    return ''.join(random.SystemRandom().choice(values) for _ in range(n))


def limit_exec_ram(limit=None):
    """
    Limit the script's execution ram so that no freezes occur. If limit is larger than available memory, warn user and
    limit to available ram instead.
    :param limit: integer,
    The number of bytes to limit ram usage to.
    :return:None
    """
    def _limit(to):
        import resource
        rsrc = resource.RLIMIT_AS
        soft, hard = resource.getrlimit(rsrc)
        print('Soft limit starts as  :', soft, hard)

        resource.setrlimit(rsrc, (to, hard))

        soft, hard = resource.getrlimit(rsrc)
        print('Soft limit changed to :', soft, hard, "\n")

    if limit:
        from psutil import virtual_memory
        mem = virtual_memory()

        # total physical memory available -
        # todo: perhaps use the available instead of total:
        # the memory that can be given instantly to processes without the system going into swap.
        print("Total Memory", mem.total)

        if limit < mem.total:
            _limit(limit)
        else:
            import warnings
            warnings.warn("You don't really have that much memory :). Limiting to {}".format(mem.total))
            _limit(mem.total)

