import pandas as pd
import numpy as np


def cv(df):
    """
    Custom cross-validation split where trainset contains only data from one class.
    Suitable for One Class Svm
    :param df:
    :return:
    """
    # randomly assign 3/4 of the feature df to training and 1/4 to test
    results = []
    # for i in xrange(0, 3):
    malicious = df[df["class"] == -1].sample(frac=1)
    malicious["is_train"] = np.random.uniform(0, 1, len(malicious)) <= .75
    normal = df[df["class"] == 1].sample(frac=1)
    normal["is_train"] = np.random.uniform(0, 1, len(normal)) <= .75
    df = pd.concat([malicious, normal], ignore_index=True)
    train, test = df[df["is_train"] == True], df[df["is_train"] == False]
    results.append([train[train["class"] == 1].index, test.index])

    return results, df


def one_class_svm_grid_search(df):
    # The C parameter trades off misclassification of training examples against simplicity of the decision surface.
    # A low C makes the decision surface smooth
    # One class SVM has a C=0.
    from sklearn.svm import OneClassSVM
    from sklearn.model_selection import GridSearchCV

    ocsvm = OneClassSVM(verbose=True, shrinking=False)
    parameters = {
        'kernel': ["linear", "rbf", "poly"],
        "degree":  [1, 2, 3, 4],
        "gamma":  [1e-1, 1, 1e1],
        "nu":  [0.9, 0.7, 0.5, 0.3, 0.1, 0.01],
    }

    cv_ind, df = cv(df)
    clf = GridSearchCV(ocsvm, parameters, scoring="accuracy", cv=cv_ind, n_jobs=3)