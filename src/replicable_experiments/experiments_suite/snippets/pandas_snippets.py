def create_corr_matrix(pd):
    import statsmodels.api as sm
    import matplotlib.pyplot as plt

    corr_df = pd.corr()
    corr_df.sort(axis=0, inplace=True)
    corr_df.sort(axis=1, inplace=True)
    corr_matrix = corr_df.as_matrix()
    sm.graphics.plot_corr(corr_matrix, ynames=corr_df.index.tolist(), xnames=corr_df.columns.tolist(), cmap='binary')
    plt.show()


def shuffle_df(df):
    return df.sample(frac=1)


def more_than_one_filtering_conditions(df, condition1, condition12):
    return df[(condition1) & (condition12)]


def sort_df(df, by):
    """
    df.sort_values(by=['orig_h', 'resp_h', 'resp_p'])
    :param df:
    :param by:
    :return:
    """
    return df.sort_values(by=by)


def load_bro_log(filename, fields_to_use, columns=None, nrows=1000000):
    import pandas as pd
    fields = columns if columns else\
            ['ts',
              'uid',
              'orig_h',
              'orig_p',
              'resp_h',
              'resp_p',
              'trans_depth',
              'method',
              'host',
              'uri',
              'referrer',
              'user_agent',
              'request_body_len',
              'response_body_len',
              'status_code',
              'status_msg',
              'info_code',
              'info_msg',
              'filename',
              'tags',
              'username',
              'password',
              'proxied orig_fuids',
              'orig_mime_types',
              'resp_fuids',
              'resp_mime_types']

    df = pd.read_csv(filename,
                     header=None,
                     sep='\t',
                     names=fields,
                     skiprows=8,
                     # skipfooter=1,
                     index_col=False,
                     quotechar=None,
                     quoting=3,
                     engine='python',
                     nrows=nrows
                     )

    return df[fields_to_use]