import re


def get_proper_table_name(cls_name):
    return "_".join([a.lower() for a in re.split(r'([A-Z][a-z]*)', cls_name) if a])