from enum import Enum


class Step(Enum):
    FeatureExtraction = "FeatureExtraction"
    Vectorization = "Vectorization"
    Transformation = "Transformation"
    Train = "Train"
    Predict = "Predict"
    Evaluate = "Evaluate"
    Save = "Save"

    def __str__(self):
        # http://stackoverflow.com/a/35964875/3433323
        return self.value

    def __eq__(self, other):
        """Override the default Equals behavior"""
        if isinstance(other, str):
            return self.value == other
        elif isinstance(other, Step):
            return self.value == other.value
        return False


class ClsKind(Enum):
    Supervised = 0
    Unsupervised = 1
    SemiSupervised = 2
    OneClass = 3
    DeepLearning = 4  # optimistic !!

    def __int__(self):
        # http://stackoverflow.com/a/35964875/3433323
        return self.value

    def __eq__(self, other):
        """Override the default Equals behavior"""
        if isinstance(other, int):
            return self.value == other
        elif isinstance(other, ClsKind):
            return self.value == other.value
        return False


class ClsType(Enum):
    LinearSvm = 0
    DecisionTree = 1
    MultinomialNB = 3
    BernoulliNB = 4
    GaussianNB = 5
    SVC = 6
    NuSVC = 7
    Svm = 8
    SVR = 9
    NuSVR = 10
    RandomForestRegressor = 11
    BayesianRidge = 12
    LogisticRegression = 13
    SGDClassifier = 14
    SGDRegressor = 15
    Perceptron = 16
    AdaBoost = 100
    Bagging = 101
    MaxVote = 102
    OneClassSvm = 200
    IsolationForest = 201
    KMeans = 202
    MiniBatchKMeans = 203

    def __int__(self):
        # http://stackoverflow.com/a/35964875/3433323
        return self.value

    def __eq__(self, other):
        """Override the default Equals behavior"""
        if isinstance(other, int):
            return self.value == other
        elif isinstance(other, ClsType):
            return self.value == other.value
        return False

# LinearSvm = "LinearSvm"
# DecisionTree = "DecisionTree"
# NBayes = "NBayes"
# Svm = "Svm"
# SVR = "SVR"
# NuSVR = "NuSVR"
# RandomForestRegressor = "RandomForestRegressor"
# IsolationForest = "IsolationForest"
# SGD = "SGD"
# Perceptron = "Perceptron"
# AdaBoost = "AdaBoost"
# Bagging = "Bagging"
# MaxVote = "MaxVote"
# OneClassSvm = "OneClassSvm"
# SVC = "SVC"


class VecType(Enum):
    Dict = 0
    Count = 1
    Tf = 2
    TfIdf = 3
    Idf = 4

    def __int__(self):
        # http://stackoverflow.com/a/35964875/3433323
        return self.value

    def __eq__(self, other):
        """Override the default Equals behavior"""
        if isinstance(other, int):
            return self.value == other
        elif isinstance(other, VecType):
            return self.value == other.value
        return False


class TransformationOptions(Enum):
    CosineSimilarity = 0
    EuclideanDist = 1
    TfIdf = 2
    PCA = 3

    def __int__(self):
        # http://stackoverflow.com/a/35964875/3433323
        return self.value

    def __eq__(self, other):
        """Override the default Equals behavior"""
        if isinstance(other, int):
            return self.value == other
        elif isinstance(other, TransformationOptions):
            return self.value == other.value
        return False


