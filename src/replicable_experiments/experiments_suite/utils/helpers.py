def divide_or_zero(a, b):
    if b > 0:
        return a / b
    return 0
