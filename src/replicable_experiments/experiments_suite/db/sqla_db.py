from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker


# engine = create_engine('sqlite:///:memory:', echo=True)
# engine = create_engine('sqlite:///../data/db/experiments.db', echo=True)
engine = create_engine('mysql+mysqldb://root:m.karanasou@localhost', echo=True)
engine.execute("CREATE DATABASE IF NOT EXISTS replicable_experiments") #create db
engine.execute("USE replicable_experiments") # select new db
Base = declarative_base()
Session = sessionmaker(bind=engine)
Session.configure(bind=engine)
session = Session()



