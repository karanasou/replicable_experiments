import json
import sqlalchemy
from sqlalchemy.ext import mutable


class JsonEncodedDict(sqlalchemy.TypeDecorator):
    """
     Enables JSON storage by encoding and decoding on the fly.
     source: http://docs.sqlalchemy.org/en/rel_1_1/orm/extensions/mutable.html,
     http://variable-scope.com/posts/creating-a-json-column-type-for-sqlalchemy
    """

    def __init__(self):
        super(JsonEncodedDict, self).__init__()

    impl = sqlalchemy.String

    def process_bind_param(self, value, dialect):
        return json.dumps(value)

    def process_result_value(self, value, dialect):
        return json.loads(value)


mutable.MutableDict.associate_with(JsonEncodedDict)
