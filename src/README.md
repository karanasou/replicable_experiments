What is this about
------------------
A framework to formalize Machine Learning experiments and store the respective results.


What do I want to do with this?
-------------------------------
-

Definitions
-----------
- Dataset

A class that holds the data as a `pandas.Dataframe`, knows how to split it and what the column that holds the class is.

- Pre-processing:

Any change done on the actual data, not creating new columns e.g. remove noise from text,
normalize dates etc.

- Feature extraction:

Create new columns in dataset e.g. POS tagged text, group dates, discretization etc.

- Vectorization:

Creating the proper input for the classifier (transforming feature values to vectors)

- Transformation:

Vector-wise transformations like pair-wise cosine similarity, euclidean distance etc.


- Algorithm Application:

Classification/ Clustering/  Regression etc.
