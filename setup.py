#!/usr/bin/env python
from __future__ import absolute_import
from setuptools import setup

REQUIREMENTS = [i.strip() for i in open("requirements.txt").readlines()]

setup(name='enorasys-sa',
      version='2017.07.b1',
      description='Replicable ML Experiments Suite',
      url='https://bitbucket.org/mkaranasou/replicable_experiments',
      maintainer='Maria Karanasou',
      maintainer_email='karanasou@gmail.com',
      author='Maria Karanasou',
      author_email='karanasou@gmail.com',
      tests_require=[
          'nose',
      ],
      test_suite='nose.collector',
      install_requires=REQUIREMENTS,
      package_dir={'': 'src'},
      packages=[
          'replicable_experiments',
          'replicable_experiments.exp_cases',
          'replicable_experiments.experiments_suite',
          'replicable_experiments.experiments_suite.db',
          'replicable_experiments.experiments_suite.models',
          'replicable_experiments.experiments_suite.snippets',
          'replicable_experiments.experiments_suite.tests',
          'replicable_experiments.experiments_suite.utils',
          'replicable_experiments.experiments_suite.visualization'
      ],
      include_package_data=True,
      dependency_links=[
          'git+ssh://git@github.com:mkaranasou/normalizer.git',
      ]
      )
